import { useState } from "react"
import "./forminputDATA.css"
import Axios from 'axios'

function Input() {

    const [start , setStart] = useState(0);
    const [end , setEnd] = useState(0);

    const [historyList , setHistoryList] = useState([]);

    const getHistory = () => {
        Axios.get('http://localhost:3001/prime_number').then((response) => {
          setHistoryList(response.data);
        });
      }

    const Calculate = (event)=>{

        let N1 = Number(start)
        let N2 = Number(end)
        let i = Number(0)
        let j = Number(0)

        for (i=N1+1; i<=N2; i++) {
            let C = Number(0);
            for (j=2;j<=i/2;j++){
                if (i%j==0){
                    C=1;
                    break;
                }
            }
            if(C==0){
                    Axios.post('http://localhost:3001/add_data' , {
                    start:Number(N1),
                    end:Number(N2),
                    prime:Number(i),
                    cprime:Number(j)
                    }).then(() => {
                      setHistoryList([
                      ...historyList,
                      {
                        start:Number(N1),
                        end:Number(N2),
                        prime:Number(i),
                        cprime:Number(j)
                      }])
                    })
            }
        }
    }

    return(
        <div>
            <form onSubmit={Calculate}>
                <div className="form-con">
                    <label>Start</label>
                    <input type="number" id="IP1" onChange={(event) => {setStart(event.target.value)}}></input>
                </div>
                <div className="form-con">
                    <label>To</label>
                    <input type="number" id="IP2" onChange={(event) => {setEnd(event.target.value)}}></input>
                </div>
                <div>
                    <button type="submit" className="btn">Calculate</button>
                </div>
            </form>

            <button className="btn" onClick={getHistory}>History Calculate</button>

            <hr></hr>

            {historyList.map((val , key) => {
            return (
              <div>
                <div className="form-con">
                  <p>input start : {val.input_start}</p>
                  <p>input end : {val.input_end}</p>
                  <p>prime number : {val.prime_number}</p>
                  <p>count prime number : {val.count_prime_number}</p>
                  <p>timestamp : {val.timestamp}</p>
                  <hr></hr>
                </div>
              </div>
            )
          })}
        </div>
    );
}

export default Input;