const express = require('express');
const app = express();
const mysql = require('mysql');
const cors = require('cors');

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
    user: "root",
    host: "localhost",
    password: "",
    database: "prime_number"
})

app.post("/add_data" , (req , res) => {
    const start = req.body.start;
    const end = req.body.end;
    const prime = req.body.prime;
    const cprime = req.body.cprime;
    
    db.query(
        "INSERT INTO history (input_start, input_end, prime_number, count_prime_number) VALUES(?,?,?,?)",
        [start, end, prime, cprime],
        (err, result) => 
            {
                if (err)
                {
                console.log(err);
                } 
                else 
                {
                    res.send("Values inserted");
                }
            }
)}
);

app.get('/prime_number' , (req , res) => {
    db.query("SELECT * FROM history" , (err , result) => {
        if (err) 
        {
            console.log(err);
        }
        else 
        {
            res.send(result);
        }
    });
});

app.listen('3001' , () => {
    console.log('Server is running on port 3001');
})